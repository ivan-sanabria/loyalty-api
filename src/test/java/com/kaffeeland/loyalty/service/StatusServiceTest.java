/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.loyalty.service;

import com.kaffeeland.loyalty.domain.Status;
import com.kaffeeland.loyalty.domain.StatusId;
import com.kaffeeland.loyalty.dto.StatusDto;
import com.kaffeeland.loyalty.repository.StatusRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;

/**
 * Class to handle test cases for status service operations: <p>
 * <p>
 * - create status for user redemption stamps <p>
 * - update status for user redemption stamps <p>
 * - find status for user redemption stamps <p>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@RunWith(SpringRunner.class)
public class StatusServiceTest {

    /**
     * Mock of status repository.
     */
    @MockBean
    private StatusRepository statusRepository;

    /**
     * Mock of the status service.
     */
    private StatusService statusService;

    /**
     * Init the necessary mocks before running test cases.
     */
    @Before
    public void initBean() {
        statusService = new StatusService(statusRepository);
    }


    @Test
    public void given_status_dto_save_return_uuid() {

        final String uuid = "12345";
        final String redemptionId = "98765";
        final Integer number = 0;

        final StatusDto statusDto = new StatusDto();

        statusDto.setUuid(uuid);
        statusDto.setRedemptionId(redemptionId);
        statusDto.setNumber(number);

        final Status status = new Status();

        status.setUuid(uuid);
        status.setRedemptionId(redemptionId);
        status.setNumber(number);

        given(statusRepository.save(status))
                .willReturn(status);

        final StatusDto result = statusService.createStatus(statusDto);
        assertEquals(statusDto, result);
    }

    @Test
    public void given_status_dto_update_number_drinks() {

        final String uuid = "12345";
        final String redemptionId = "98765";
        final Integer number = 0;

        final Integer updatedNumber = 5;

        final StatusDto statusDto = new StatusDto();
        statusDto.setNumber(updatedNumber);

        final StatusId statusId = new StatusId();

        statusId.setUuid(uuid);
        statusId.setRedemptionId(redemptionId);

        final Status storeStatus = new Status();

        storeStatus.setUuid(uuid);
        storeStatus.setRedemptionId(redemptionId);
        storeStatus.setNumber(number);

        given(statusRepository.findById(statusId))
                .willReturn(Optional.of(storeStatus));

        final Status updateStatus = new Status();

        updateStatus.setUuid(uuid);
        updateStatus.setRedemptionId(redemptionId);
        updateStatus.setNumber(updatedNumber);

        given(statusRepository.save(updateStatus))
                .willReturn(updateStatus);

        final StatusDto result = statusService.updateStatus(uuid, redemptionId, statusDto);

        assertEquals(uuid, result.getUuid());
        assertEquals(redemptionId, result.getRedemptionId());
        assertEquals(updatedNumber, result.getNumber());
    }

    @Test
    public void given_status_dto_update_not_found_record() {

        final String uuid = "12345";
        final String redemptionId = "98765";

        final Integer updatedNumber = 5;

        final StatusDto statusDto = new StatusDto();
        statusDto.setNumber(updatedNumber);

        final StatusId statusId = new StatusId();

        statusId.setUuid(uuid);
        statusId.setRedemptionId(redemptionId);

        given(statusRepository.findById(statusId))
                .willReturn(Optional.empty());

        final StatusDto result = statusService.updateStatus(uuid, redemptionId, statusDto);
        assertNull(result);
    }

    @Test
    public void given_valid_uuid_redemption_id_find_status() {

        final String uuid = "12345";
        final String redemptionId = "98765";
        final Integer number = 5;

        final StatusId statusId = new StatusId();

        statusId.setUuid(uuid);
        statusId.setRedemptionId(redemptionId);

        final Status storeStatus = new Status();

        storeStatus.setUuid(uuid);
        storeStatus.setRedemptionId(redemptionId);
        storeStatus.setNumber(number);

        given(statusRepository.findById(statusId))
                .willReturn(Optional.of(storeStatus));

        final StatusDto statusDto = statusService.findStatusByUuidAndRedemptionId(uuid, redemptionId);

        assertEquals(uuid, statusDto.getUuid());
        assertEquals(redemptionId, statusDto.getRedemptionId());
        assertEquals(number, statusDto.getNumber());
    }

    @Test
    public void given_valid_uuid_redemption_id_return_null() {

        final String uuid = "12345";
        final String redemptionId = "98765";

        final StatusId statusId = new StatusId();

        statusId.setUuid(uuid);
        statusId.setRedemptionId(redemptionId);

        given(statusRepository.findById(statusId))
                .willReturn(Optional.empty());

        final StatusDto statusDto = statusService.findStatusByUuidAndRedemptionId(uuid, redemptionId);
        assertNull(statusDto);
    }

}
