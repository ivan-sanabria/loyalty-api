/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.loyalty.service;

import com.kaffeeland.loyalty.domain.Redemption;
import com.kaffeeland.loyalty.dto.RedemptionDto;
import com.kaffeeland.loyalty.repository.RedemptionRepository;
import com.kaffeeland.loyalty.util.RedemptionState;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;

/**
 * Class to handle test cases for redemption service operations: <p>
 * <p>
 * - create redemption for coffee shops <p>
 * - update redemption for coffee shops <p>
 * - find redemption for coffee shops <p>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@RunWith(SpringRunner.class)
public class RedemptionServiceTest {

    /**
     * Mock of redemption repository.
     */
    @MockBean
    private RedemptionRepository redemptionRepository;

    /**
     * Mock of the redemption service.
     */
    private RedemptionService redemptionService;

    /**
     * Init the necessary mocks before running test cases.
     */
    @Before
    public void initBean() {
        redemptionService = new RedemptionService(redemptionRepository);
    }


    @Test
    public void given_redemption_dto_save_return_uuid() {

        final String uuid = "12345";
        final Long expiration = System.currentTimeMillis();
        final Integer number = 0;
        final String placeId = "Avengers";

        final RedemptionDto redemptionDto = new RedemptionDto();

        redemptionDto.setExpiration(expiration);
        redemptionDto.setNumber(number);
        redemptionDto.setState(RedemptionState.OPEN.name());
        redemptionDto.setPlaceId(placeId);

        final Redemption redemption = new Redemption();

        redemption.setExpiration(expiration);
        redemption.setNumber(number);
        redemption.setState(RedemptionState.OPEN.ordinal());
        redemption.setPlaceId(placeId);

        final Redemption storeRedemption = new Redemption();

        storeRedemption.setUuid(uuid);
        storeRedemption.setExpiration(expiration);
        storeRedemption.setNumber(number);
        storeRedemption.setState(RedemptionState.OPEN.ordinal());
        storeRedemption.setPlaceId(placeId);

        given(redemptionRepository.save(redemption))
                .willReturn(storeRedemption);

        final String createdUuid = redemptionService.createRedemption(redemptionDto);
        assertEquals(uuid, createdUuid);
    }

    @Test
    public void given_redemption_dto_update_number_drinks() {

        final String uuid = "12345";
        final Long expiration = System.currentTimeMillis();
        final Integer number = 0;
        final String placeId = "Avengers";

        final Integer updatedNumber = 5;

        final RedemptionDto redemptionDto = new RedemptionDto();
        redemptionDto.setNumber(updatedNumber);

        final Redemption storeRedemption = new Redemption();

        storeRedemption.setUuid(uuid);
        storeRedemption.setExpiration(expiration);
        storeRedemption.setNumber(number);
        storeRedemption.setState(RedemptionState.OPEN.ordinal());
        storeRedemption.setPlaceId(placeId);

        given(redemptionRepository.findOptionalByUuid(uuid))
                .willReturn(Optional.of(storeRedemption));

        final Redemption updatedRedemption = new Redemption();

        updatedRedemption.setUuid(uuid);
        updatedRedemption.setExpiration(expiration);
        updatedRedemption.setNumber(updatedNumber);
        updatedRedemption.setState(RedemptionState.OPEN.ordinal());
        updatedRedemption.setPlaceId(placeId);

        given(redemptionRepository.save(updatedRedemption))
                .willReturn(updatedRedemption);

        final RedemptionDto updatedRedemptionDto = redemptionService.updateRedemption(uuid, redemptionDto);

        assertEquals(uuid, updatedRedemptionDto.getUuid());
        assertEquals(expiration, updatedRedemptionDto.getExpiration());
        assertEquals(updatedNumber, updatedRedemptionDto.getNumber());
        assertEquals(placeId, updatedRedemptionDto.getPlaceId());
        assertEquals(RedemptionState.OPEN.name(), updatedRedemptionDto.getState());
    }

    @Test
    public void given_redemption_dto_update_place_id() {

        final String uuid = "12345";
        final Long expiration = System.currentTimeMillis();
        final Integer number = 0;
        final String placeId = "Avengers";

        final String updatedPlaceId = "Big Bang";

        final RedemptionDto redemptionDto = new RedemptionDto();
        redemptionDto.setPlaceId(updatedPlaceId);

        final Redemption storeRedemption = new Redemption();

        storeRedemption.setUuid(uuid);
        storeRedemption.setExpiration(expiration);
        storeRedemption.setNumber(number);
        storeRedemption.setState(RedemptionState.OPEN.ordinal());
        storeRedemption.setPlaceId(placeId);

        given(redemptionRepository.findOptionalByUuid(uuid))
                .willReturn(Optional.of(storeRedemption));

        final Redemption updatedRedemption = new Redemption();

        updatedRedemption.setUuid(uuid);
        updatedRedemption.setExpiration(expiration);
        updatedRedemption.setNumber(number);
        updatedRedemption.setState(RedemptionState.OPEN.ordinal());
        updatedRedemption.setPlaceId(updatedPlaceId);

        given(redemptionRepository.save(updatedRedemption))
                .willReturn(updatedRedemption);

        final RedemptionDto updatedRedemptionDto = redemptionService.updateRedemption(uuid, redemptionDto);

        assertEquals(uuid, updatedRedemptionDto.getUuid());
        assertEquals(expiration, updatedRedemptionDto.getExpiration());
        assertEquals(number, updatedRedemptionDto.getNumber());
        assertEquals(updatedPlaceId, updatedRedemptionDto.getPlaceId());
        assertEquals(RedemptionState.OPEN.name(), updatedRedemptionDto.getState());
    }

    @Test
    public void given_redemption_dto_update_state() {

        final String uuid = "12345";
        final Long expiration = System.currentTimeMillis();
        final Integer number = 0;
        final String placeId = "Avengers";

        final String updateState = RedemptionState.DELETED.name();

        final RedemptionDto redemptionDto = new RedemptionDto();
        redemptionDto.setState(updateState);

        final Redemption storeRedemption = new Redemption();

        storeRedemption.setUuid(uuid);
        storeRedemption.setExpiration(expiration);
        storeRedemption.setNumber(number);
        storeRedemption.setState(RedemptionState.OPEN.ordinal());
        storeRedemption.setPlaceId(placeId);

        given(redemptionRepository.findOptionalByUuid(uuid))
                .willReturn(Optional.of(storeRedemption));

        final Redemption updatedRedemption = new Redemption();

        updatedRedemption.setUuid(uuid);
        updatedRedemption.setExpiration(expiration);
        updatedRedemption.setNumber(number);
        updatedRedemption.setState(RedemptionState.DELETED.ordinal());
        updatedRedemption.setPlaceId(placeId);

        given(redemptionRepository.save(updatedRedemption))
                .willReturn(updatedRedemption);

        final RedemptionDto updatedRedemptionDto = redemptionService.updateRedemption(uuid, redemptionDto);

        assertEquals(uuid, updatedRedemptionDto.getUuid());
        assertEquals(expiration, updatedRedemptionDto.getExpiration());
        assertEquals(number, updatedRedemptionDto.getNumber());
        assertEquals(placeId, updatedRedemptionDto.getPlaceId());
        assertEquals(RedemptionState.DELETED.name(), updatedRedemptionDto.getState());
    }

    @Test
    public void given_redemption_dto_update_not_found_record() {

        final String uuid = "12345";

        final Integer updatedNumber = 5;

        final RedemptionDto redemptionDto = new RedemptionDto();
        redemptionDto.setNumber(updatedNumber);

        given(redemptionRepository.findOptionalByUuid(uuid))
                .willReturn(Optional.empty());

        final RedemptionDto updatedRedemptionDto = redemptionService.updateRedemption(uuid, redemptionDto);
        assertNull(updatedRedemptionDto);
    }

    @Test
    public void given_valid_uuid_find_redemption() {

        final String uuid = "12345";
        final Long expiration = System.currentTimeMillis();
        final Integer number = 0;
        final String placeId = "Avengers";

        final Redemption storeRedemption = new Redemption();

        storeRedemption.setUuid(uuid);
        storeRedemption.setExpiration(expiration);
        storeRedemption.setNumber(number);
        storeRedemption.setState(RedemptionState.OPEN.ordinal());
        storeRedemption.setPlaceId(placeId);

        given(redemptionRepository.findOptionalByUuid(uuid))
                .willReturn(Optional.of(storeRedemption));

        final RedemptionDto redemptionDto = redemptionService.findRedemptionByUuid(uuid);

        assertNotNull(redemptionDto);
        assertEquals(uuid, redemptionDto.getUuid());
        assertEquals(expiration, redemptionDto.getExpiration());
        assertEquals(number, redemptionDto.getNumber());
        assertEquals(placeId, redemptionDto.getPlaceId());
        assertEquals(RedemptionState.OPEN.name(), redemptionDto.getState());
    }

    @Test
    public void given_valid_uuid_find_return_null() {

        final String uuid = "12345";

        given(redemptionRepository.findOptionalByUuid(uuid))
                .willReturn(Optional.empty());

        final RedemptionDto redemptionDto = redemptionService.findRedemptionByUuid(uuid);
        assertNull(redemptionDto);
    }

}

