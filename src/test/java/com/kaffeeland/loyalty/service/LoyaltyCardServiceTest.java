/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.loyalty.service;

import com.kaffeeland.loyalty.domain.LoyaltyCard;
import com.kaffeeland.loyalty.domain.LoyaltyCardId;
import com.kaffeeland.loyalty.dto.LoyaltyCardDto;
import com.kaffeeland.loyalty.repository.LoyaltyCardRepository;
import com.kaffeeland.loyalty.util.LoyaltyCardState;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;

/**
 * Class to handle test cases for loyalty service operations: <p>
 * <p>
 * - create loyalty card for given application <p>
 * - update loyalty card for given application <p>
 * - delete loyalty card for given application <p>
 * - find loyalty card for given application <p>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@RunWith(SpringRunner.class)
public class LoyaltyCardServiceTest {

    /**
     * Mock of loyalty card repository.
     */
    @MockBean
    private LoyaltyCardRepository loyaltyCardRepository;

    /**
     * Mock of loyalty card service.
     */
    private LoyaltyCardService loyaltyCardService;

    /**
     * Init the necessary mocks before running test cases.
     */
    @Before
    public void initBean() {
        loyaltyCardService = new LoyaltyCardService(loyaltyCardRepository);
    }


    @Test
    public void given_loyalty_card_dto_save_return_uuid() {

        final String application = "kaffeeland-ios";
        final String uuid = "12345";
        final String userId = "12345";
        final Long expedition = System.currentTimeMillis();

        final LoyaltyCardDto loyaltyCardDto = new LoyaltyCardDto();

        loyaltyCardDto.setState(LoyaltyCardState.CREATED.name());
        loyaltyCardDto.setUserId(userId);
        loyaltyCardDto.setExpedition(expedition);

        final LoyaltyCard loyaltyCard = new LoyaltyCard();

        loyaltyCard.setApplication(application);
        loyaltyCard.setState(LoyaltyCardState.CREATED.ordinal());
        loyaltyCard.setUserId(userId);
        loyaltyCard.setExpedition(expedition);

        final LoyaltyCard storeLoyaltyCard = new LoyaltyCard();

        storeLoyaltyCard.setUuid(uuid);
        storeLoyaltyCard.setApplication(application);
        storeLoyaltyCard.setState(LoyaltyCardState.CREATED.ordinal());
        storeLoyaltyCard.setUserId(userId);

        given(loyaltyCardRepository.save(loyaltyCard))
                .willReturn(storeLoyaltyCard);

        final String createdUuid = loyaltyCardService.createLoyaltyCard(loyaltyCardDto, application);
        assertEquals(uuid, createdUuid);
    }

    @Test
    public void given_loyalty_card_dto_update_expedition() {

        final String application = "kaffeeland-ios";
        final String uuid = "12345";
        final String userId = "12345";

        final Long expedition = System.currentTimeMillis();

        final LoyaltyCardDto loyaltyCardDto = new LoyaltyCardDto();
        loyaltyCardDto.setExpedition(expedition);

        final LoyaltyCardId loyaltyCardId = new LoyaltyCardId();

        loyaltyCardId.setApplication(application);
        loyaltyCardId.setUuid(uuid);

        final LoyaltyCard storeLoyaltyCard = new LoyaltyCard();

        storeLoyaltyCard.setUuid(uuid);
        storeLoyaltyCard.setApplication(application);
        storeLoyaltyCard.setState(LoyaltyCardState.CREATED.ordinal());
        storeLoyaltyCard.setUserId(userId);

        given(loyaltyCardRepository.findById(loyaltyCardId))
                .willReturn(Optional.of(storeLoyaltyCard));

        final LoyaltyCard updatedLoyaltyCard = new LoyaltyCard();

        updatedLoyaltyCard.setUuid(uuid);
        updatedLoyaltyCard.setApplication(application);
        updatedLoyaltyCard.setState(LoyaltyCardState.CREATED.ordinal());
        updatedLoyaltyCard.setUserId(userId);
        updatedLoyaltyCard.setExpedition(expedition);

        given(loyaltyCardRepository.save(updatedLoyaltyCard))
                .willReturn(updatedLoyaltyCard);

        final LoyaltyCardDto updatedLoyaltyCardDto = loyaltyCardService.updateLoyaltyCard(uuid, loyaltyCardDto, application);

        assertEquals(uuid, updatedLoyaltyCardDto.getUuid());
        assertEquals(userId, updatedLoyaltyCardDto.getUserId());
        assertEquals(expedition, updatedLoyaltyCardDto.getExpedition());
        assertEquals(LoyaltyCardState.CREATED.name(), updatedLoyaltyCardDto.getState());
    }

    @Test
    public void given_loyalty_card_dto_update_userId() {

        final String application = "kaffeeland-ios";
        final String uuid = "12345";
        final String userId = "12345";

        final String updateUserId = "98765";

        final LoyaltyCardDto loyaltyCardDto = new LoyaltyCardDto();
        loyaltyCardDto.setUserId(updateUserId);

        final LoyaltyCardId loyaltyCardId = new LoyaltyCardId();

        loyaltyCardId.setApplication(application);
        loyaltyCardId.setUuid(uuid);

        final LoyaltyCard storeLoyaltyCard = new LoyaltyCard();

        storeLoyaltyCard.setUuid(uuid);
        storeLoyaltyCard.setApplication(application);
        storeLoyaltyCard.setState(LoyaltyCardState.CREATED.ordinal());
        storeLoyaltyCard.setUserId(userId);

        given(loyaltyCardRepository.findById(loyaltyCardId))
                .willReturn(Optional.of(storeLoyaltyCard));

        final LoyaltyCard updatedLoyaltyCard = new LoyaltyCard();

        updatedLoyaltyCard.setUuid(uuid);
        updatedLoyaltyCard.setApplication(application);
        updatedLoyaltyCard.setState(LoyaltyCardState.CREATED.ordinal());
        updatedLoyaltyCard.setUserId(updateUserId);

        given(loyaltyCardRepository.save(updatedLoyaltyCard))
                .willReturn(updatedLoyaltyCard);

        final LoyaltyCardDto updatedLoyaltyCardDto = loyaltyCardService.updateLoyaltyCard(uuid, loyaltyCardDto, application);

        assertEquals(uuid, updatedLoyaltyCardDto.getUuid());
        assertEquals(updateUserId, updatedLoyaltyCardDto.getUserId());
        assertEquals(LoyaltyCardState.CREATED.name(), updatedLoyaltyCardDto.getState());
    }

    @Test
    public void given_loyalty_card_dto_update_state() {

        final String application = "kaffeeland-ios";
        final String uuid = "12345";
        final String userId = "12345";

        final String updateState = LoyaltyCardState.APPROVED.name();

        final LoyaltyCardDto loyaltyCardDto = new LoyaltyCardDto();
        loyaltyCardDto.setState(updateState);

        final LoyaltyCardId loyaltyCardId = new LoyaltyCardId();

        loyaltyCardId.setApplication(application);
        loyaltyCardId.setUuid(uuid);

        final LoyaltyCard storeLoyaltyCard = new LoyaltyCard();

        storeLoyaltyCard.setUuid(uuid);
        storeLoyaltyCard.setApplication(application);
        storeLoyaltyCard.setState(LoyaltyCardState.CREATED.ordinal());
        storeLoyaltyCard.setUserId(userId);

        given(loyaltyCardRepository.findById(loyaltyCardId))
                .willReturn(Optional.of(storeLoyaltyCard));

        final LoyaltyCard updatedLoyaltyCard = new LoyaltyCard();

        updatedLoyaltyCard.setUuid(uuid);
        updatedLoyaltyCard.setApplication(application);
        updatedLoyaltyCard.setState(LoyaltyCardState.APPROVED.ordinal());
        updatedLoyaltyCard.setUserId(userId);

        given(loyaltyCardRepository.save(updatedLoyaltyCard))
                .willReturn(updatedLoyaltyCard);

        final LoyaltyCardDto updatedLoyaltyCardDto = loyaltyCardService.updateLoyaltyCard(uuid, loyaltyCardDto, application);

        assertEquals(uuid, updatedLoyaltyCardDto.getUuid());
        assertEquals(userId, updatedLoyaltyCardDto.getUserId());
        assertEquals(LoyaltyCardState.APPROVED.name(), updatedLoyaltyCardDto.getState());
    }

    @Test
    public void given_loyalty_card_dto_update_not_found_record() {

        final String application = "kaffeeland-ios";
        final String uuid = "12345";

        final Long expedition = System.currentTimeMillis();

        final LoyaltyCardDto loyaltyCardDto = new LoyaltyCardDto();
        loyaltyCardDto.setExpedition(expedition);

        final LoyaltyCardId loyaltyCardId = new LoyaltyCardId();

        loyaltyCardId.setApplication(application);
        loyaltyCardId.setUuid(uuid);

        given(loyaltyCardRepository.findById(loyaltyCardId))
                .willReturn(Optional.empty());

        final LoyaltyCardDto updatedLoyaltyCardDto = loyaltyCardService.updateLoyaltyCard(uuid, loyaltyCardDto, application);
        assertNull(updatedLoyaltyCardDto);
    }

    @Test
    public void given_valid_uuid_application_delete_loyalty_card_successfully() {

        final String application = "kaffeeland-ios";
        final String uuid = "12345";
        final String userId = "12345";

        final LoyaltyCardId loyaltyCardId = new LoyaltyCardId();

        loyaltyCardId.setApplication(application);
        loyaltyCardId.setUuid(uuid);

        final LoyaltyCard storeLoyaltyCard = new LoyaltyCard();

        storeLoyaltyCard.setUuid(uuid);
        storeLoyaltyCard.setApplication(application);
        storeLoyaltyCard.setState(LoyaltyCardState.CREATED.ordinal());
        storeLoyaltyCard.setUserId(userId);

        given(loyaltyCardRepository.findById(loyaltyCardId))
                .willReturn(Optional.of(storeLoyaltyCard));

        doNothing()
                .when(loyaltyCardRepository).deleteById(loyaltyCardId);

        final String deleteUuid = loyaltyCardService.deleteLoyaltyCard(uuid, application);
        assertEquals(uuid, deleteUuid);
    }

    @Test
    public void given_valid_uuid_application_delete_loyalty_card_not_found() {

        final String application = "kaffeeland-ios";
        final String uuid = "12345";

        final LoyaltyCardId loyaltyCardId = new LoyaltyCardId();

        loyaltyCardId.setApplication(application);
        loyaltyCardId.setUuid(uuid);

        given(loyaltyCardRepository.findById(loyaltyCardId))
                .willReturn(Optional.empty());

        final String deleteUuid = loyaltyCardService.deleteLoyaltyCard(uuid, application);
        assertNull(deleteUuid);
    }

    @Test
    public void given_valid_uuid_application_find_return_loyalty_card() {

        final String application = "kaffeeland-ios";
        final String uuid = "12345";
        final String userId = "12345";
        final Long expedition = System.currentTimeMillis();

        final LoyaltyCardId loyaltyCardId = new LoyaltyCardId();

        loyaltyCardId.setApplication(application);
        loyaltyCardId.setUuid(uuid);

        final LoyaltyCard storeLoyaltyCard = new LoyaltyCard();

        storeLoyaltyCard.setUuid(uuid);
        storeLoyaltyCard.setApplication(application);
        storeLoyaltyCard.setState(LoyaltyCardState.CREATED.ordinal());
        storeLoyaltyCard.setUserId(userId);
        storeLoyaltyCard.setExpedition(expedition);

        given(loyaltyCardRepository.findById(loyaltyCardId))
                .willReturn(Optional.of(storeLoyaltyCard));

        final LoyaltyCardDto loyaltyCardDto = loyaltyCardService.findLoyaltyCardByUuid(uuid, application);

        assertNotNull(loyaltyCardDto);
        assertEquals(uuid, loyaltyCardDto.getUuid());
        assertEquals(userId, loyaltyCardDto.getUserId());
        assertEquals(expedition, loyaltyCardDto.getExpedition());
        assertEquals(LoyaltyCardState.CREATED.name(), loyaltyCardDto.getState());
    }

    @Test
    public void given_valid_uuid_application_find_return_null() {

        final String application = "kaffeeland-ios";
        final String uuid = "12345";

        final LoyaltyCardId loyaltyCardId = new LoyaltyCardId();

        loyaltyCardId.setApplication(application);
        loyaltyCardId.setUuid(uuid);

        given(loyaltyCardRepository.findById(loyaltyCardId))
                .willReturn(Optional.empty());

        final LoyaltyCardDto loyaltyCardDto = loyaltyCardService.findLoyaltyCardByUuid(uuid, application);
        assertNull(loyaltyCardDto);
    }

}
