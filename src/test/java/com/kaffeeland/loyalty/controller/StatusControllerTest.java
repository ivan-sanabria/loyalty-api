/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.loyalty.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kaffeeland.loyalty.dto.StatusDto;
import com.kaffeeland.loyalty.service.StatusService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Class to handle test cases for status controller operations: <p>
 * <p>
 * - creates the status of a redemption for specific user <p>
 * - updates the status of a redemption for specific user <p>
 * - retrieves the status of a redemption for specific user <p>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.1.0
 */
@RunWith(SpringRunner.class)
@WebMvcTest(StatusController.class)
public class StatusControllerTest {

    /**
     * Define path to test controller.
     */
    private static final String PATH = "/status";

    /**
     * Mock of redemption service.
     */
    @MockBean
    private StatusService statusService;

    /**
     * Mock of MVC to perform HTTP requests.
     */
    @Autowired
    private MockMvc mvc;

    /**
     * Object Mapper used to write JSON response data into content bodies.
     */
    @Autowired
    private ObjectMapper objectMapper;


    @Test
    public void post_given_status_return_status_ok() throws Exception {

        final String uuid = "121345";
        final String redemptionId = "98765";

        final StatusDto statusDto = new StatusDto();
        statusDto.setUuid(uuid);
        statusDto.setRedemptionId(redemptionId);

        final String expectedResponse = objectMapper.writeValueAsString(statusDto);

        given(statusService.createStatus(statusDto))
                .willReturn(statusDto);

        mvc.perform(post(PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(statusDto)))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }

    @Test
    public void post_given_status_return_status_internal_server_error() throws Exception {

        final StatusDto statusDto = new StatusDto();

        given(statusService.createStatus(statusDto))
                .willReturn(null);

        mvc.perform(post(PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(statusDto)))
                .andExpect(status().isInternalServerError());

        given(statusService.createStatus(statusDto))
                .willReturn(null);

        mvc.perform(post(PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(statusDto)))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void put_given_status_return_status_ok() throws Exception {

        final String uuid = "121345";
        final String redemptionId = "987565";

        final StatusDto expectedStatusDto = new StatusDto();
        expectedStatusDto.setUuid(uuid);
        expectedStatusDto.setRedemptionId(redemptionId);

        final String expectedResponse = objectMapper.writeValueAsString(expectedStatusDto);

        final StatusDto statusDto = new StatusDto();

        given(statusService.updateStatus(uuid, redemptionId, statusDto))
                .willReturn(expectedStatusDto);

        mvc.perform(put(PATH + "/" + uuid + "/" + redemptionId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(statusDto)))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }

    @Test
    public void put_given_status_return_status_not_found() throws Exception {

        final String uuid = "121345";
        final String redemptionId = "987565";

        final StatusDto statusDto = new StatusDto();

        given(statusService.updateStatus(uuid, redemptionId, statusDto))
                .willReturn(null);

        mvc.perform(put(PATH + "/" + uuid + "/" + redemptionId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(statusDto)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void retrieve_given_status_return_status_ok() throws Exception {

        final String uuid = "121345";
        final String redemptionId = "987565";

        final StatusDto expectedStatusDto = new StatusDto();
        expectedStatusDto.setUuid(uuid);
        expectedStatusDto.setRedemptionId(redemptionId);

        final String expectedResponse = objectMapper.writeValueAsString(expectedStatusDto);

        given(statusService.findStatusByUuidAndRedemptionId(uuid, redemptionId))
                .willReturn(expectedStatusDto);

        mvc.perform(get(PATH + "/" + uuid + "/" + redemptionId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }

    @Test
    public void retrieve_given_status_return_status_not_found() throws Exception {

        final String uuid = "121345";
        final String redemptionId = "987565";

        given(statusService.findStatusByUuidAndRedemptionId(uuid, redemptionId))
                .willReturn(null);

        mvc.perform(get(PATH + "/" + uuid + "/" + redemptionId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

}
