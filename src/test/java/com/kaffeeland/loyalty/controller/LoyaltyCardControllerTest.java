/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.loyalty.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kaffeeland.loyalty.dto.LoyaltyCardDto;
import com.kaffeeland.loyalty.service.LoyaltyCardService;
import com.kaffeeland.loyalty.util.LoyaltyCardHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Random;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Class to handle test cases for loyalty controller operations: <p>
 * <p>
 * - creates the loyalty card of the user for specific app <p>
 * - updates the loyalty card of the user for specific app <p>
 * - deletes the loyalty card of the user for specific app <p>
 * - retrieves the loyalty card of the user for specific app <p>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@RunWith(SpringRunner.class)
@WebMvcTest(LoyaltyCardController.class)
public class LoyaltyCardControllerTest {

    /**
     * Define path to test controller.
     */
    private static final String PATH = "/loyalty";

    /**
     * Define the header name for application
     */
    private static final String APPLICATION_HEADER = "application";

    /**
     * Mock of loyalty card service.
     */
    @MockBean
    private LoyaltyCardService loyaltyCardService;

    /**
     * Mock of loyalty card helper.
     */
    @MockBean
    private LoyaltyCardHelper loyaltyCardHelper;

    /**
     * Mock of MVC to perform HTTP requests.
     */
    @Autowired
    private MockMvc mvc;

    /**
     * Object Mapper used to write JSON request / response data into content bodies.
     */
    @Autowired
    private ObjectMapper objectMapper;


    @Test
    public void post_given_loyalty_card_return_status_ok() throws Exception {

        final String expectedUuid = "121345";
        final byte[] expectedQrCode = new byte[20];
        new Random().nextBytes(expectedQrCode);

        final String application = "kaffeeland-ios";
        final LoyaltyCardDto loyaltyCardDto = new LoyaltyCardDto();

        given(loyaltyCardService.createLoyaltyCard(loyaltyCardDto, application))
                .willReturn(expectedUuid);

        given(loyaltyCardHelper.generateQrCode(eq(expectedUuid), anyInt(), anyInt(), anyString()))
                .willReturn(expectedQrCode);

        mvc.perform(post(PATH)
                .header(APPLICATION_HEADER, application)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loyaltyCardDto)))
                .andExpect(status().isOk())
                .andExpect(content().bytes(expectedQrCode));
    }

    @Test
    public void post_given_loyalty_card_return_internal_server_error() throws Exception {

        final String application = "kaffeeland-ios";
        final LoyaltyCardDto loyaltyCardDto = new LoyaltyCardDto();

        given(loyaltyCardService.createLoyaltyCard(loyaltyCardDto, application))
                .willReturn(null);

        mvc.perform(post(PATH)
                .header(APPLICATION_HEADER, application)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loyaltyCardDto)))
                .andExpect(status().isInternalServerError());

        given(loyaltyCardService.createLoyaltyCard(loyaltyCardDto, application))
                .willReturn("");

        mvc.perform(post(PATH)
                .header(APPLICATION_HEADER, application)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loyaltyCardDto)))
                .andExpect(status().isInternalServerError());

        final String expectedUuid = "12345";

        given(loyaltyCardService.createLoyaltyCard(loyaltyCardDto, application))
                .willReturn(expectedUuid);

        given(loyaltyCardHelper.generateQrCode(eq(expectedUuid), anyInt(), anyInt(), anyString()))
                .willReturn(null);

        mvc.perform(post(PATH)
                .header(APPLICATION_HEADER, application)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loyaltyCardDto)))
                .andExpect(status().isInternalServerError());

        given(loyaltyCardHelper.generateQrCode(eq(expectedUuid), anyInt(), anyInt(), anyString()))
                .willReturn(new byte[0]);

        mvc.perform(post(PATH)
                .header(APPLICATION_HEADER, application)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loyaltyCardDto)))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void put_given_loyalty_card_return_status_ok() throws Exception {

        final String uuid = "121345";
        final String application = "kaffeeland-ios";

        final LoyaltyCardDto expectedLoyaltyCard = new LoyaltyCardDto();
        expectedLoyaltyCard.setUuid(uuid);

        final String expectedResponse = objectMapper.writeValueAsString(expectedLoyaltyCard);

        final LoyaltyCardDto loyaltyCardDto = new LoyaltyCardDto();

        given(loyaltyCardService.updateLoyaltyCard(uuid, loyaltyCardDto, application))
                .willReturn(expectedLoyaltyCard);

        mvc.perform(put(PATH + "/" + uuid)
                .header(APPLICATION_HEADER, application)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loyaltyCardDto)))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }

    @Test
    public void put_given_loyalty_card_return_status_not_found() throws Exception {

        final String uuid = "121345";
        final String application = "kaffeeland-ios";
        final LoyaltyCardDto loyaltyCardDto = new LoyaltyCardDto();

        given(loyaltyCardService.updateLoyaltyCard(uuid, loyaltyCardDto, application))
                .willReturn(null);

        mvc.perform(put(PATH + "/" + uuid)
                .header(APPLICATION_HEADER, application)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loyaltyCardDto)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void delete_given_loyalty_card_return_status_ok() throws Exception {

        final String uuid = "121345";
        final String application = "kaffeeland-ios";

        given(loyaltyCardService.deleteLoyaltyCard(uuid, application))
                .willReturn(uuid);

        mvc.perform(delete(PATH + "/" + uuid)
                .header(APPLICATION_HEADER, application)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void delete_given_loyalty_card_return_status_not_found() throws Exception {

        final String uuid = "121345";
        final String application = "kaffeeland-ios";

        given(loyaltyCardService.deleteLoyaltyCard(uuid, application))
                .willReturn(null);

        mvc.perform(delete(PATH + "/" + uuid)
                .header(APPLICATION_HEADER, application)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        given(loyaltyCardService.deleteLoyaltyCard(uuid, application))
                .willReturn("");

        mvc.perform(delete(PATH + "/" + uuid)
                .header(APPLICATION_HEADER, application)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void retrieve_given_loyalty_card_return_status_ok() throws Exception {

        final String uuid = "121345";
        final String application = "kaffeeland-ios";

        final LoyaltyCardDto expectedLoyaltyCard = new LoyaltyCardDto();
        expectedLoyaltyCard.setUuid(uuid);

        final byte[] expectedQrCode = new byte[20];
        new Random().nextBytes(expectedQrCode);

        given(loyaltyCardService.findLoyaltyCardByUuid(uuid, application))
                .willReturn(expectedLoyaltyCard);

        given(loyaltyCardHelper.generateQrCode(eq(uuid), anyInt(), anyInt(), anyString()))
                .willReturn(expectedQrCode);

        mvc.perform(get(PATH + "/" + uuid)
                .header(APPLICATION_HEADER, application)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().bytes(expectedQrCode));
    }

    @Test
    public void retrieve_given_loyalty_card_return_status_not_found() throws Exception {

        final String uuid = "121345";
        final String application = "kaffeeland-ios";

        given(loyaltyCardService.findLoyaltyCardByUuid(uuid, application))
                .willReturn(null);

        mvc.perform(get(PATH + "/" + uuid)
                .header(APPLICATION_HEADER, application)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void retrieve_given_loyalty_card_return_status_internal_server_error() throws Exception {

        final String uuid = "121345";
        final String application = "kaffeeland-ios";

        final LoyaltyCardDto expectedLoyaltyCard = new LoyaltyCardDto();
        expectedLoyaltyCard.setUuid(uuid);

        given(loyaltyCardService.findLoyaltyCardByUuid(uuid, application))
                .willReturn(expectedLoyaltyCard);

        given(loyaltyCardHelper.generateQrCode(eq(uuid), anyInt(), anyInt(), anyString()))
                .willReturn(null);

        mvc.perform(get(PATH + "/" + uuid)
                .header(APPLICATION_HEADER, application)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());

        given(loyaltyCardHelper.generateQrCode(eq(uuid), anyInt(), anyInt(), anyString()))
                .willReturn(new byte[0]);

        mvc.perform(get(PATH + "/" + uuid)
                .header(APPLICATION_HEADER, application)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

}
