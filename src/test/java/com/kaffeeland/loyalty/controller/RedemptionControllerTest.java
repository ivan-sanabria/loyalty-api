/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.loyalty.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kaffeeland.loyalty.dto.RedemptionDto;
import com.kaffeeland.loyalty.service.RedemptionService;
import com.kaffeeland.loyalty.util.RedemptionState;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Map;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Class to handle test cases for redemption controller operations: <p>
 * <p>
 * - creates the redemption for coffee shops <p>
 * - updates the redemption for coffee shops <p>
 * - deletes the redemption for coffee shops <p>
 * - retrieves the redemption for coffee shops <p>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.1.0
 */
@RunWith(SpringRunner.class)
@WebMvcTest(RedemptionController.class)
public class RedemptionControllerTest {

    /**
     * Define path to test controller.
     */
    private static final String PATH = "/redemption";

    /**
     * Mock of redemption service.
     */
    @MockBean
    private RedemptionService redemptionService;

    /**
     * Mock of MVC to perform HTTP requests.
     */
    @Autowired
    private MockMvc mvc;

    /**
     * Object Mapper used to write JSON response data into content bodies.
     */
    @Autowired
    private ObjectMapper objectMapper;


    @Test
    public void post_given_redemption_return_status_ok() throws Exception {

        final String expectedUuid = "121345";

        final Map<String, String> response = Collections.singletonMap("uuid", expectedUuid);
        final String expectedResponse = objectMapper.writeValueAsString(response);

        final RedemptionDto redemptionDto = new RedemptionDto();

        given(redemptionService.createRedemption(redemptionDto))
                .willReturn(expectedUuid);

        mvc.perform(post(PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(redemptionDto)))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }

    @Test
    public void post_given_redemption_return_internal_server_error() throws Exception {

        final RedemptionDto redemptionDto = new RedemptionDto();

        given(redemptionService.createRedemption(redemptionDto))
                .willReturn(null);

        mvc.perform(post(PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(redemptionDto)))
                .andExpect(status().isInternalServerError());

        given(redemptionService.createRedemption(redemptionDto))
                .willReturn("");

        mvc.perform(post(PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(redemptionDto)))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void put_given_redemption_return_status_ok() throws Exception {

        final String uuid = "121345";

        final RedemptionDto expectedRedemption = new RedemptionDto();
        expectedRedemption.setUuid(uuid);

        final String expectedResponse = objectMapper.writeValueAsString(expectedRedemption);

        final RedemptionDto redemptionDto = new RedemptionDto();

        given(redemptionService.updateRedemption(uuid, redemptionDto))
                .willReturn(expectedRedemption);

        mvc.perform(put(PATH + "/" + uuid)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(redemptionDto)))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }

    @Test
    public void put_given_redemption_return_status_not_found() throws Exception {

        final String uuid = "121345";
        final RedemptionDto redemptionDto = new RedemptionDto();

        given(redemptionService.updateRedemption(uuid, redemptionDto))
                .willReturn(null);

        mvc.perform(put(PATH + "/" + uuid)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(redemptionDto)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void delete_given_redemption_return_status_ok() throws Exception {

        final String uuid = "121345";

        final RedemptionDto expectedRedemption = new RedemptionDto();
        expectedRedemption.setUuid(uuid);

        given(redemptionService.findRedemptionByUuid(uuid))
                .willReturn(expectedRedemption);

        expectedRedemption.setState(RedemptionState.DELETED.name());

        given(redemptionService.updateRedemption(uuid, expectedRedemption))
                .willReturn(expectedRedemption);

        mvc.perform(delete(PATH + "/" + uuid)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void delete_given_redemption_return_status_not_found() throws Exception {

        final String uuid = "121345";

        final RedemptionDto redemptionDto = new RedemptionDto();

        given(redemptionService.findRedemptionByUuid(uuid))
                .willReturn(null);

        mvc.perform(delete(PATH + "/" + uuid)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(redemptionDto)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void retrieve_given_redemption_return_status_ok() throws Exception {

        final String uuid = "121345";

        final RedemptionDto expectedRedemption = new RedemptionDto();
        expectedRedemption.setUuid(uuid);

        final String expectedResponse = objectMapper.writeValueAsString(expectedRedemption);

        given(redemptionService.findRedemptionByUuid(uuid))
                .willReturn(expectedRedemption);

        mvc.perform(get(PATH + "/" + uuid)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }

    @Test
    public void retrieve_given_redemption_return_status_not_found() throws Exception {

        final String uuid = "121345";

        given(redemptionService.findRedemptionByUuid(uuid))
                .willReturn(null);

        mvc.perform(get(PATH + "/" + uuid)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

}
