/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.loyalty.util;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Class to handle test cases for qr code generation: <p>
 * <p>
 * - generate qr code for given key <p>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class LoyaltyCardHelperTest {

    /**
     * Mock of loyalty card helper.
     */
    private LoyaltyCardHelper loyaltyCardHelper;

    /**
     * Init the necessary mocks before running test cases.
     */
    @Before
    public void initBean() {
        loyaltyCardHelper = new LoyaltyCardHelper();
    }


    @Test
    public void given_proper_params_return_byte_array() {

        final String key = "12345";
        final int width = 250;
        final int height = 250;
        final String imageType = "jpeg";

        final byte[] result = loyaltyCardHelper.generateQrCode(key, width, height, imageType);
        assertNotNull(result);
    }

    @Test
    public void given_proper_params_except_image_type_return_null() {

        final String key = "12345";
        final int width = 250;
        final int height = 250;
        final String imageType = "not-supported";

        final byte[] result = loyaltyCardHelper.generateQrCode(key, width, height, imageType);
        assertEquals(0, result.length);
    }

    @Test
    public void given_proper_params_except_image_dimensions_return_null() {

        final String key = "12345";
        final int width = 0;
        final int height = 0;
        final String imageType = "not-supported";

        final byte[] result = loyaltyCardHelper.generateQrCode(key, width, height, imageType);
        assertEquals(0, result.length);
    }

    @Test
    public void given_proper_params_except_key_return_null() {

        final String key = "";
        final int width = 0;
        final int height = 0;
        final String imageType = "not-supported";

        final byte[] result = loyaltyCardHelper.generateQrCode(key, width, height, imageType);
        assertEquals(0, result.length);
    }

}
