/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.loyalty.util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

/**
 * Util class responsible of generating QR code for loyalty cards.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Configuration
public class LoyaltyCardHelper {

    /**
     * Define logger instance to track errors.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(LoyaltyCardHelper.class);

    /**
     * Generate a byte array of QR codes using given key.
     *
     * @param key       Key to generate QR code.
     * @param width     With of the image of the QR code.
     * @param height    Height of the image of the QR code.
     * @param imageType Image type to generate the QR code. Example: jpeg.
     * @return Array of bytes representing the image of QR code.
     */
    public byte[] generateQrCode(final String key, final int width, final int height, final String imageType) {

        final QRCodeWriter qrCodeWriter = new QRCodeWriter();
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        try {

            final BitMatrix bitMatrix = qrCodeWriter.encode(
                    key,
                    BarcodeFormat.QR_CODE,
                    width,
                    height);

            MatrixToImageWriter.writeToStream(bitMatrix, imageType, outputStream);

            return outputStream.toByteArray();

        } catch (IOException io) {

            LOGGER.error("Error writing the QR code image into bytes.", io);

        } catch (Exception e) {

            LOGGER.error("Error generating QR code with the given key {}.", key, e);
        }

        return new byte[0];
    }

}
