/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.loyalty.service;

import com.kaffeeland.loyalty.domain.Status;
import com.kaffeeland.loyalty.domain.StatusId;
import com.kaffeeland.loyalty.dto.StatusDto;
import com.kaffeeland.loyalty.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Class responsible of handling status of a redemption logic covering the following use cases:
 * <p>
 * - create status for user redemption stamps <p>
 * - update status for user redemption stamps <p>
 * - find status for user redemption stamps <p>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.1.0
 */
@Service
public class StatusService {

    /**
     * Status repository used to persist data into dynamo db.
     */
    private final StatusRepository statusRepository;

    /**
     * Constructor of the status service used to inject repository.
     *
     * @param statusRepository Status repository to persist data into dynamo db.
     */
    @Autowired
    public StatusService(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    /**
     * Create status of a redemption for specific loyalty card using the given data transfer object.
     *
     * @param statusDto Status data transfer object to store it in datasource.
     * @return Unique distributed identifier of the status.
     */
    public StatusDto createStatus(final StatusDto statusDto) {

        final Status status = new Status();

        status.setUuid(
                statusDto.getUuid());

        status.setRedemptionId(
                statusDto.getRedemptionId());

        status.setNumber(
                statusDto.getNumber());

        return parseDto(statusRepository.save(status));
    }

    /**
     * Update status using the given unique distributed identifier of the loyalty card and redemption with the given data
     * transfer object.
     *
     * @param uuid         Unique distributed identifier of the loyalty card.
     * @param redemptionId Unique distributed identifier of the redemption.
     * @param statusDto    Status data transfer object to update in datasource.
     * @return Status data transfer instance with the updated data.
     */
    public StatusDto updateStatus(final String uuid, final String redemptionId, final StatusDto statusDto) {

        final StatusId statusId = new StatusId();

        statusId.setUuid(uuid);
        statusId.setRedemptionId(redemptionId);

        final Optional<Status> optionalStatus = statusRepository.findById(statusId);

        return optionalStatus.map(status ->
                parseDto(mergeToDomain(statusDto, status)))
                .orElse(null);
    }

    /**
     * Find redemption status using the given unique identifier of the loyalty card and redemption.
     *
     * @param uuid         Unique distributed identifier of the loyalty card.
     * @param redemptionId Unique distributed identifier of the redemption.
     * @return Status data transfer instance found on datasource.
     */
    public StatusDto findStatusByUuidAndRedemptionId(final String uuid, final String redemptionId) {

        final StatusId statusId = new StatusId();

        statusId.setUuid(uuid);
        statusId.setRedemptionId(redemptionId);

        final Optional<Status> status = statusRepository.findById(statusId);

        return status.map(this::parseDto)
                .orElse(null);
    }

    /**
     * Merge the data transfer object into the given domain object.
     *
     * @param statusDto Data transfer object to merge data into domain object.
     * @param status    Domain object to receive data from data transfer object.
     * @return Domain object with the merge result.
     */
    private Status mergeToDomain(final StatusDto statusDto, final Status status) {

        status.setNumber(statusDto.getNumber());

        return statusRepository.save(status);
    }

    /**
     * Parse the given domain object to data transfer object.
     *
     * @param status Domain object to parse.
     * @return Data transfer object with the given domain data.
     */
    private StatusDto parseDto(final Status status) {

        return new StatusDto(
                status.getUuid(),
                status.getRedemptionId(),
                status.getNumber());
    }

}
