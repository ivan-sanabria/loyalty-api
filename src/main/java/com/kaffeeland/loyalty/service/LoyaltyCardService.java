/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.loyalty.service;

import com.kaffeeland.loyalty.domain.LoyaltyCard;
import com.kaffeeland.loyalty.domain.LoyaltyCardId;
import com.kaffeeland.loyalty.dto.LoyaltyCardDto;
import com.kaffeeland.loyalty.repository.LoyaltyCardRepository;
import com.kaffeeland.loyalty.util.LoyaltyCardState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Class responsible of handling loyalty cards business logic covering the following use cases:
 * <p>
 * - create loyalty card for given application <p>
 * - update loyalty card for given application <p>
 * - delete loyalty card for given application <p>
 * - find loyalty card for given application <p>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Service
public class LoyaltyCardService {

    /**
     * Loyalty card repository used to persist data into dynamo db.
     */
    private final LoyaltyCardRepository loyaltyCardRepository;

    /**
     * Constructor of the loyalty card service used to inject repository.
     *
     * @param loyaltyCardRepository Loyalty card repository to persist data into dynamo db.
     */
    @Autowired
    public LoyaltyCardService(LoyaltyCardRepository loyaltyCardRepository) {
        this.loyaltyCardRepository = loyaltyCardRepository;
    }

    /**
     * Create loyalty card using the given data transfer object and application.
     *
     * @param loyaltyCardDto Loyalty card data transfer object to store it in datasource.
     * @param application    Application is provider of the loyalty card.
     * @return Unique distributed identifier of the loyalty card.
     */
    public String createLoyaltyCard(final LoyaltyCardDto loyaltyCardDto, final String application) {

        final LoyaltyCardState loyaltyCardState = LoyaltyCardState.valueOf(
                loyaltyCardDto.getState());

        final LoyaltyCard loyaltyCard = new LoyaltyCard();
        loyaltyCard.setApplication(application);

        loyaltyCard.setExpedition(
                loyaltyCardDto.getExpedition());

        loyaltyCard.setUserId(
                loyaltyCardDto.getUserId());

        loyaltyCard.setState(
                loyaltyCardState.ordinal());

        return loyaltyCardRepository.save(loyaltyCard)
                .getUuid();
    }

    /**
     * Update loyalty card using the given unique distributed identifier, data transfer object and application.
     *
     * @param uuid           Unique distributed identifier to update the loyalty card.
     * @param loyaltyCardDto Loyalty card data transfer object to used to update it in datasource.
     * @param application    Application is provider of the loyalty card.
     * @return Loyalty card data transfer instance with the updated data.
     */
    public LoyaltyCardDto updateLoyaltyCard(final String uuid, final LoyaltyCardDto loyaltyCardDto,
                                            final String application) {

        final LoyaltyCardId loyaltyCardId = new LoyaltyCardId();

        loyaltyCardId.setUuid(uuid);
        loyaltyCardId.setApplication(application);

        final Optional<LoyaltyCard> optionalLoyaltyCard = loyaltyCardRepository.findById(loyaltyCardId);

        return optionalLoyaltyCard.map(loyaltyCard ->
                parseDto(mergeToDomain(loyaltyCardDto, loyaltyCard)))
                .orElse(null);
    }

    /**
     * Delete loyalty card using the given unique distributed identifier and application.
     *
     * @param uuid        Unique distributed identifier to delete the loyalty card.
     * @param application Application is provider of the loyalty card.
     * @return Unique distributed identifier of the loyalty card that was deleted.
     */
    public String deleteLoyaltyCard(final String uuid, final String application) {

        final LoyaltyCardId loyaltyCardId = new LoyaltyCardId();

        loyaltyCardId.setUuid(uuid);
        loyaltyCardId.setApplication(application);

        final Optional<LoyaltyCard> optionalLoyaltyCard = loyaltyCardRepository.findById(loyaltyCardId);

        if (optionalLoyaltyCard.isPresent()) {

            loyaltyCardRepository.deleteById(loyaltyCardId);

            return optionalLoyaltyCard.get()
                    .getUuid();

        } else {

            return null;
        }
    }

    /**
     * Find loyalty card using the given unique distributed identifier and application.
     *
     * @param uuid        Unique distributed identifier to search the loyalty card.
     * @param application Application is provider of the loyalty card.
     * @return Loyalty card data transfer instance found on datasource.
     */
    public LoyaltyCardDto findLoyaltyCardByUuid(final String uuid, final String application) {

        final LoyaltyCardId loyaltyCardId = new LoyaltyCardId();

        loyaltyCardId.setUuid(uuid);
        loyaltyCardId.setApplication(application);

        final Optional<LoyaltyCard> optionalLoyaltyCard = loyaltyCardRepository.findById(loyaltyCardId);

        return optionalLoyaltyCard.map(this::parseDto)
                .orElse(null);
    }

    /**
     * Merge the data transfer object data into the given domain object and persist the result to the datasource.
     *
     * @param loyaltyCardDto Data transfer object to merge data into domain object.
     * @param loyaltyCard    Domain object to receive data from data transfer object.
     * @return Domain object with the merge result.
     */
    private LoyaltyCard mergeToDomain(final LoyaltyCardDto loyaltyCardDto, final LoyaltyCard loyaltyCard) {

        loyaltyCard.setUserId(
                (null == loyaltyCardDto.getUserId()) ?
                        loyaltyCard.getUserId() : loyaltyCardDto.getUserId());

        loyaltyCard.setExpedition(
                (null == loyaltyCardDto.getExpedition()) ?
                        loyaltyCard.getExpedition() : loyaltyCardDto.getExpedition());

        if (null != loyaltyCardDto.getState()) {

            final LoyaltyCardState loyaltyCardState = LoyaltyCardState.valueOf(
                    loyaltyCardDto.getState());

            loyaltyCard.setState(
                    loyaltyCardState.ordinal());
        }

        return loyaltyCardRepository.save(loyaltyCard);
    }

    /**
     * Parse the given domain object to data transfer object.
     *
     * @param loyaltyCard Domain object to parse.
     * @return Data transfer object with the given domain data.
     */
    private LoyaltyCardDto parseDto(final LoyaltyCard loyaltyCard) {

        final LoyaltyCardState[] states = LoyaltyCardState.values();
        final Integer currentState = loyaltyCard.getState();
        final String loyaltyCardState = states[currentState].name();

        return new LoyaltyCardDto(
                loyaltyCard.getUuid(),
                loyaltyCard.getUserId(),
                loyaltyCard.getExpedition(),
                loyaltyCardState);
    }

}
