/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.loyalty.service;

import com.kaffeeland.loyalty.domain.Redemption;
import com.kaffeeland.loyalty.dto.RedemptionDto;
import com.kaffeeland.loyalty.repository.RedemptionRepository;
import com.kaffeeland.loyalty.util.RedemptionState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Class responsible of handling redemption business logic covering the following use cases:
 * <p>
 * - create redemption for coffee shops <p>
 * - update redemption for coffee shops <p>
 * - find redemption for coffee shops <p>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.1.0
 */
@Service
public class RedemptionService {

    /**
     * Redemption repository used to persist data into dynamo db.
     */
    private final RedemptionRepository redemptionRepository;

    /**
     * Constructor of the redemption service used to inject repository.
     *
     * @param redemptionRepository Redemption repository to persist data into dynamo db.
     */
    @Autowired
    public RedemptionService(RedemptionRepository redemptionRepository) {
        this.redemptionRepository = redemptionRepository;
    }

    /**
     * Create redemption using the given data transfer object.
     *
     * @param redemptionDto Redemption data transfer object to store it in datasource.
     * @return Unique distributed identifier of the redemption.
     */
    public String createRedemption(final RedemptionDto redemptionDto) {

        final Redemption redemption = new Redemption();

        redemption.setExpiration(
                redemptionDto.getExpiration());

        redemption.setNumber(
                redemptionDto.getNumber());

        redemption.setState(RedemptionState.OPEN
                .ordinal());

        redemption.setPlaceId(
                redemptionDto.getPlaceId());

        return redemptionRepository.save(redemption)
                .getUuid();
    }

    /**
     * Update redemption using the given unique distributed identifier and data transfer object.
     *
     * @param uuid          Unique distributed identifier to update the redemption.
     * @param redemptionDto Redemption data transfer object to update it in datasource.
     * @return Redemption data transfer instance with the updated data.
     */
    public RedemptionDto updateRedemption(final String uuid, final RedemptionDto redemptionDto) {

        final Optional<Redemption> optionalRedemption = redemptionRepository.findOptionalByUuid(uuid);

        return optionalRedemption.map(redemption ->
                parseDto(mergeToDomain(redemptionDto, redemption)))
                .orElse(null);
    }

    /**
     * Find redemption using the given unique identifier.
     *
     * @param uuid Unique distributed identifier of the redemption.
     * @return RedemptionDto data transfer instance found on datasource.
     */
    public RedemptionDto findRedemptionByUuid(final String uuid) {

        final Optional<Redemption> redemption =
                redemptionRepository.findOptionalByUuid(uuid);

        return redemption.map(this::parseDto)
                .orElse(null);
    }

    /**
     * Merge the data transfer object into the given domain object.
     *
     * @param redemptionDto Data transfer object to merge data into domain object.
     * @param redemption    Domain object to receive data from data transfer object.
     * @return Domain object with the merge result.
     */
    private Redemption mergeToDomain(final RedemptionDto redemptionDto, final Redemption redemption) {

        redemption.setNumber(
                (null == redemptionDto.getNumber()) ?
                        redemption.getNumber() : redemptionDto.getNumber());

        redemption.setPlaceId(
                (null == redemptionDto.getPlaceId()) ?
                        redemption.getPlaceId() : redemptionDto.getPlaceId());

        if (null != redemptionDto.getState()) {

            final RedemptionState redemptionState = RedemptionState.valueOf(
                    redemptionDto.getState());

            redemption.setState(
                    redemptionState.ordinal());
        }

        return redemptionRepository.save(redemption);
    }

    /**
     * Parse the given domain object to data transfer object.
     *
     * @param redemption Domain object to parse.
     * @return Data transfer object with the given domain data.
     */
    private RedemptionDto parseDto(final Redemption redemption) {

        final RedemptionState[] states = RedemptionState.values();
        final Integer currentState = redemption.getState();
        final String redemptionState = states[currentState].name();

        return new RedemptionDto(
                redemption.getUuid(),
                redemption.getExpiration(),
                redemption.getNumber(),
                redemptionState,
                redemption.getPlaceId());
    }

}
