/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.loyalty.controller;

import com.kaffeeland.loyalty.dto.RedemptionDto;
import com.kaffeeland.loyalty.service.RedemptionService;
import com.kaffeeland.loyalty.util.RedemptionState;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

/**
 * Class responsible of handling request made to /redemption endpoint supporting:
 * <p>
 * - creates the redemption for coffee shops <p>
 * - updates the redemption for coffee shops <p>
 * - deletes the redemption for coffee shops <p>
 * - retrieves the redemption for coffee shops <p>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.1.0
 */
@RestController
@Tag(name = "Redemption Endpoints", description = "Supported operations to redeem rewards based on loyalty card records.")
public class RedemptionController {

    /**
     * Service to expose redemption operations.
     */
    private final RedemptionService redemptionService;

    /**
     * Constructor of the redemption controller to inject required services.
     *
     * @param redemptionService Redemption service instance used to handle redemption operations.
     */
    @Autowired
    public RedemptionController(RedemptionService redemptionService) {
        this.redemptionService = redemptionService;
    }

    /**
     * Method to handle post requests made to /redemption endpoint.
     *
     * @param redemptionDto Redemption data to store.
     * @return Response entity with the unique distribute identifier of the redemption, otherwise INTERNAL_SERVER_ERROR is returned.
     */
    @Operation(description = "Create redemption data used on kaffeeland services.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully create redemption data on the system.",
                    content = @Content(schema = @Schema(implementation = RedemptionDto.class))),
            @ApiResponse(responseCode = "400",
                    description = "The given json is not parsable to a redemption data.",
                    content = @Content),
            @ApiResponse(responseCode = "415",
                    description = "Given content-type on the request was not correct. Expected is application/json.",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Server error occurs by processing the redemption request.",
                    content = @Content)
    })
    @PostMapping(value = "/redemption", produces = "application/json")
    public ResponseEntity<Map<String, String>> createRedemption(@RequestBody RedemptionDto redemptionDto) {

        final String uuid = redemptionService.createRedemption(redemptionDto);

        if (null == uuid || uuid.isEmpty())
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        final Map<String, String> response = Collections.singletonMap("uuid", uuid);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    /**
     * Method to handle put request to /redemption/uuid endpoint.
     *
     * @param uuid          Unique distributed identifier of the redemption to update.
     * @param redemptionDto Redemption data to update.
     * @return Response entity with the updated redemption data, otherwise NOT_FOUND is returned.
     */
    @Operation(description = "Update redemption used on kaffeeland services.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully update a redemption on the system.",
                    content = @Content(schema = @Schema(implementation = RedemptionDto.class))),
            @ApiResponse(responseCode = "400",
                    description = "The given json is not parsable to a redemption.",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "A redemption with the given unique identifier was not found.",
                    content = @Content),
            @ApiResponse(responseCode = "415",
                    description = "Given content-type on the request was not correct. Expected is application/json.",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Server error occurs by processing the redemption request.",
                    content = @Content)
    })
    @PutMapping(value = "/redemption/{uuid}", produces = "application/json")
    public ResponseEntity<RedemptionDto> updateRedemption(@Parameter(description = "Unique distributed identifier of the redemption.")
                                                          @PathVariable(value = "uuid") String uuid,
                                                          @RequestBody RedemptionDto redemptionDto) {

        final RedemptionDto redemptionDtoUpdated =
                redemptionService.updateRedemption(uuid, redemptionDto);

        if (null == redemptionDtoUpdated)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(redemptionDtoUpdated, HttpStatus.OK);
    }

    /**
     * Method to handle delete request to /redemption/uuid endpoint.
     *
     * @param uuid Unique distributed identifier of the redemption to delete.
     * @return Response entity with OK, otherwise NOT_FOUND is returned.
     */
    @Operation(description = "Delete redemption used on kaffeeland services.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully delete a redemption on the system.",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "A redemption with the given unique identifier was not found.",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Server error occurs by processing the redemption request.",
                    content = @Content)
    })
    @DeleteMapping(value = "/redemption/{uuid}", produces = "application/json")
    public ResponseEntity<Void> deleteRedemptionCard(@Parameter(description = "Unique distributed identifier of the redemption.")
                                                     @PathVariable(value = "uuid") String uuid) {

        final RedemptionDto redemptionDto = redemptionService.findRedemptionByUuid(uuid);

        if (null == redemptionDto)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        redemptionDto.setState(RedemptionState.DELETED
                .name());

        redemptionService.updateRedemption(uuid, redemptionDto);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Method to handle get requests made to /redemption/uuid endpoint.
     *
     * @param uuid Unique distributed identifier of the redemption.
     * @return Response entity with the redemption data, otherwise NOT_FOUND and message is returned.
     */
    @Operation(description = "Retrieves redemption data used on kaffeeland services.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully get redemption data from the system.",
                    content = @Content(schema = @Schema(implementation = RedemptionDto.class))),
            @ApiResponse(responseCode = "404",
                    description = "Redemption status data was not found with the given unique identifier.",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Server error occurs by processing the redemption request.",
                    content = @Content)
    })
    @GetMapping(value = "/redemption/{uuid}", produces = "application/json")
    public ResponseEntity<RedemptionDto> getRedemptionByUuid(@Parameter(description = "Unique distributed identifier of the redemption.")
                                                             @PathVariable(value = "uuid") String uuid) {

        final RedemptionDto redemptionDto = redemptionService.findRedemptionByUuid(uuid);

        if (null == redemptionDto)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(redemptionDto, HttpStatus.OK);
    }

}
