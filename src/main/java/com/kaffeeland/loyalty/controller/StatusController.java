/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.loyalty.controller;

import com.kaffeeland.loyalty.dto.StatusDto;
import com.kaffeeland.loyalty.service.StatusService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Class responsible of handling request made to /status endpoint supporting:
 * <p>
 * - creates the status of a redemption for specific user <p>
 * - updates the status of a redemption for specific user <p>
 * - retrieves the status of a redemption for specific user <p>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.1.0
 */
@RestController
@Tag(name = "Status Endpoints", description = "Supported operations to handle status based on loyalty card and redemption records.")
public class StatusController {

    /**
     * Service to expose loyalty cards status operations.
     */
    private final StatusService statusService;

    /**
     * Constructor of the status controller to inject required services.
     *
     * @param statusService Status service instance used to handle loyalty card status operations.
     */
    @Autowired
    public StatusController(StatusService statusService) {
        this.statusService = statusService;
    }

    /**
     * Method to handle post requests made to /status endpoint.
     *
     * @param statusDto Status data to store.
     * @return Response entity with the unique distribute identifier of the status, otherwise INTERNAL_SERVER_ERROR is returned.
     */
    @Operation(description = "Create status data used on kaffeeland services.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully create status data on the system.",
                    content = @Content(schema = @Schema(implementation = StatusDto.class))),
            @ApiResponse(responseCode = "400",
                    description = "The given json is not parsable to a status data.",
                    content = @Content),
            @ApiResponse(responseCode = "415",
                    description = "Given content-type on the request was not correct. Expected is application/json.",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Server error occurs by processing the status request.",
                    content = @Content)
    })
    @PostMapping(value = "/status", produces = "application/json")
    public ResponseEntity<StatusDto> createStatus(@RequestBody StatusDto statusDto) {

        final StatusDto result = statusService.createStatus(statusDto);

        if (null == result)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Method to handle put request to /status/uuid/redemptionId endpoint.
     *
     * @param uuid         Unique distributed identifier of the loyalty card to make the update.
     * @param redemptionId Redemption identifier to make the update.
     * @param statusDto    Status data to update.
     * @return Response entity with the updated status data, otherwise NOT_FOUND is returned.
     */
    @Operation(description = "Update status used on kaffeeland services.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully update the status on the system.",
                    content = @Content(schema = @Schema(implementation = StatusDto.class))),
            @ApiResponse(responseCode = "400",
                    description = "The given json is not parsable to a status.",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "A status with the given unique loyalty card identifier and redemption identifier was not found.",
                    content = @Content),
            @ApiResponse(responseCode = "415",
                    description = "Given content-type on the request was not correct. Expected is application/json.",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Server error occurs by processing the status request.",
                    content = @Content)
    })
    @PutMapping(value = "/status/{uuid}/{redemptionId}", produces = "application/json")
    public ResponseEntity<StatusDto> updateStatus(@Parameter(description = "Unique distributed identifier of the loyalty card.")
                                                  @PathVariable(value = "uuid") String uuid,
                                                  @Parameter(description = "Unique distributed identifier of the redemption.")
                                                  @PathVariable(value = "redemptionId") String redemptionId,
                                                  @RequestBody StatusDto statusDto) {

        final StatusDto statusDtoUpdated = statusService.updateStatus(uuid, redemptionId, statusDto);

        if (null == statusDtoUpdated)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(statusDtoUpdated, HttpStatus.OK);
    }

    /**
     * Method to handle get requests made to /status/uuid/redemptionId endpoint.
     *
     * @param uuid         Unique distributed identifier of the loyalty card to filter the search.
     * @param redemptionId Redemption identifier to filter the search.
     * @return Response entity with the status data, otherwise NOT_FOUND and message is returned.
     */
    @Operation(description = "Retrieves status data used on kaffeeland services.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully get status data from the system.",
                    content = @Content(schema = @Schema(implementation = StatusDto.class))),
            @ApiResponse(responseCode = "404",
                    description = "Status was not found with the given unique identifier loyalty card and redemption identifier.",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Server error occurs by processing the status request.",
                    content = @Content)
    })
    @GetMapping(value = "/status/{uuid}/{redemptionId}", produces = "application/json")
    public ResponseEntity<StatusDto> getStatusByUuid(@Parameter(description = "Unique distributed identifier of the loyalty card.")
                                                     @PathVariable(value = "uuid") String uuid,
                                                     @Parameter(description = "Unique distributed identifier of the redemption.")
                                                     @PathVariable(value = "redemptionId") String redemptionId) {

        final StatusDto statusDto = statusService.findStatusByUuidAndRedemptionId(uuid, redemptionId);

        if (null == statusDto)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(statusDto, HttpStatus.OK);
    }

}
