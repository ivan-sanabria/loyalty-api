/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.loyalty.controller;

import com.kaffeeland.loyalty.dto.LoyaltyCardDto;
import com.kaffeeland.loyalty.service.LoyaltyCardService;
import com.kaffeeland.loyalty.util.LoyaltyCardHelper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

/**
 * Class responsible of handling request made to /loyalty endpoint supporting:
 * <p>
 * - creates the loyalty card of the user for specific app <p>
 * - updates the loyalty card of the user for specific app <p>
 * - deletes the loyalty card of the user for specific app <p>
 * - retrieves the loyalty card of the user for specific app <p>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@RestController
@Tag(name = "Loyalty Cards Endpoints", description = "Supported operations for loyalty cards.")
public class LoyaltyCardController {

    /**
     * Service to expose loyalty cards operations.
     */
    private final LoyaltyCardService loyaltyCardService;

    /**
     * Helper of loyalty card used to generate QR codes.
     */
    private final LoyaltyCardHelper loyaltyCardHelper;

    /**
     * Height of the QR code image.
     */
    @Value("${qr.height}")
    private Integer height;

    /**
     * Width of the QR code image.
     */
    @Value("${qr.width}")
    private Integer width;

    /**
     * Constructor of the loyalty card controller to inject required services.
     *
     * @param loyaltyCardService Loyalty service instance used to handle loyalty card operations.
     * @param loyaltyCardHelper  Loyalty card helper instance used to create QR codes.
     */
    @Autowired
    public LoyaltyCardController(LoyaltyCardService loyaltyCardService, LoyaltyCardHelper loyaltyCardHelper) {
        this.loyaltyCardService = loyaltyCardService;
        this.loyaltyCardHelper = loyaltyCardHelper;
    }

    /**
     * Method to handle post request to /loyalty endpoint.
     *
     * @param application    Application is owner of the loyalty card.
     * @param loyaltyCardDto Loyalty card data to store.
     * @return Response entity with the unique distribute identifier of the loyalty card, otherwise INTERNAL_SERVER_ERROR is returned.
     */
    @Operation(description = "Create loyalty card used on kaffeeland services.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully create a loyalty card on the system.",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "The given json is not parsable to a loyalty card.",
                    content = @Content),
            @ApiResponse(responseCode = "415",
                    description = "Given content-type on the request was not correct. Expected is application/json.",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Server error occurs by processing the loyalty card request.",
                    content = @Content)
    })
    @PostMapping(value = "/loyalty", produces = "image/jpeg")
    public ResponseEntity<byte[]> createLoyaltyCard(@Parameter(description = "Application name that is creating the loyalty card.")
                                                    @RequestHeader String application,
                                                    @RequestBody LoyaltyCardDto loyaltyCardDto) {

        final MediaType mediaType = MediaType.IMAGE_JPEG;

        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(mediaType);

        final String uuid = loyaltyCardService.createLoyaltyCard(loyaltyCardDto, application);

        if (null == uuid || uuid.isEmpty())
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        final byte[] qrCode = loyaltyCardHelper.generateQrCode(
                uuid,
                width,
                height,
                mediaType.getSubtype());

        if (null == qrCode || 0 == qrCode.length)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        return new ResponseEntity<>(qrCode, httpHeaders, HttpStatus.OK);
    }

    /**
     * Method to handle put request to /loyalty/uuid endpoint.
     *
     * @param uuid           Unique distributed identifier of the loyalty card to update.
     * @param application    Application is requesting the update operation.
     * @param loyaltyCardDto Loyalty card data to update.
     * @return Response entity with the updated loyalty card data, otherwise NOT_FOUND is returned.
     */
    @Operation(description = "Update loyalty card used on kaffeeland services.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully update a loyalty card on the system.",
                    content = @Content(schema = @Schema(implementation = LoyaltyCardDto.class))),
            @ApiResponse(responseCode = "400",
                    description = "The given json is not parsable to a loyalty card.",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "A loyalty card with the given unique identifier and application was not found.",
                    content = @Content),
            @ApiResponse(responseCode = "415",
                    description = "Given content-type on the request was not correct. Expected is application/json.",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Server error occurs by processing the loyalty card request.",
                    content = @Content)
    })
    @PutMapping(value = "/loyalty/{uuid}", produces = "application/json")
    public ResponseEntity<LoyaltyCardDto> updateLoyaltyCard(@Parameter(description = "Unique distributed identifier of the loyalty card.")
                                                            @PathVariable(value = "uuid") String uuid,
                                                            @Parameter(description = "Application name that is updating the loyalty card.")
                                                            @RequestHeader String application,
                                                            @RequestBody LoyaltyCardDto loyaltyCardDto) {

        final LoyaltyCardDto loyaltyCardDtoUpdated =
                loyaltyCardService.updateLoyaltyCard(uuid, loyaltyCardDto, application);

        if (null == loyaltyCardDtoUpdated)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(loyaltyCardDtoUpdated, HttpStatus.OK);
    }

    /**
     * Method to handle delete request to /loyalty/uuid endpoint.
     *
     * @param uuid        Unique distributed identifier of the loyalty card to delete.
     * @param application Application is requesting the delete operation.
     * @return Response entity with OK, otherwise NOT_FOUND is returned.
     */
    @Operation(description = "Delete loyalty card used on kaffeeland services.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully delete a loyalty card on the system.",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "A loyalty card with the given unique identifier was not found.",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Server error occurs by processing the loyalty card request.",
                    content = @Content)
    })
    @DeleteMapping(value = "/loyalty/{uuid}", produces = "application/json")
    public ResponseEntity<Void> deleteLoyaltyCard(@Parameter(description = "Unique distributed identifier of the loyalty card.")
                                                  @PathVariable(value = "uuid") String uuid,
                                                  @Parameter(description = "Application name that is deleting the loyalty card.")
                                                  @RequestHeader String application) {

        final String uuidDeleted = loyaltyCardService.deleteLoyaltyCard(uuid, application);

        if (null == uuidDeleted || uuidDeleted.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Method to handle get requests made to /loyalty/uuid endpoint.
     *
     * @param uuid        Unique distributed identifier of the loyalty card to get.
     * @param application Application is requesting the get operation.
     * @return Response entity with the loyalty card data, otherwise NOT_FOUND and message is returned.
     */
    @Operation(description = "Retrieves loyalty card used on kaffeeland services.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully get a loyalty card from the system.",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Loyalty card was not found with the given unique identifier.",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Server error occurs by processing the loyalty card request.",
                    content = @Content)
    })
    @GetMapping(value = "/loyalty/{uuid}", produces = "image/jpeg")
    public ResponseEntity<byte[]> getLoyaltyCardByUuid(@Parameter(description = "Unique distributed identifier of the loyalty card.")
                                                       @PathVariable(value = "uuid") String uuid,
                                                       @Parameter(description = "Application name that is retrieving the loyalty card.")
                                                       @RequestHeader String application) {

        final MediaType mediaType = MediaType.IMAGE_JPEG;

        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(mediaType);

        final LoyaltyCardDto loyaltyCardDto = loyaltyCardService.findLoyaltyCardByUuid(uuid, application);

        if (null == loyaltyCardDto)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        final byte[] qrCode = loyaltyCardHelper.generateQrCode(
                loyaltyCardDto.getUuid(),
                width,
                height,
                mediaType.getSubtype());

        if (null == qrCode || 0 == qrCode.length)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        return new ResponseEntity<>(qrCode, httpHeaders, HttpStatus.OK);
    }

}
