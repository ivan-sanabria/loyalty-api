/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.loyalty.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class to encapsulate the loyalty card data supported by the loyalty endpoints.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Loyalty card data transfer object used to establish communication between the clients " +
        "and backend services.", name = "Loyalty Card")
public class LoyaltyCardDto {

    /**
     * Unique distributed identifier.
     */
    @Schema(description = "Unique distributed identifier of the loyalty card.",
            example = "123e4567-e89b-12d3-a456-426655440000")
    private String uuid;

    /**
     * User identifier owner of the loyalty card.
     */
    @Schema(description = "Unique distribute identifier of the owner of the loyalty card.",
            example = "123e4567-e89b-12d3-a456-426655440000")
    private String userId;

    /**
     * Date the loyalty card is created.
     */
    @Schema(description = "Unix time when the card was created.",
            example = "1563485352883")
    private Long expedition;

    /**
     * State of loyalty card.
     */
    @Schema(description = "State of the loyalty card.",
            example = "CREATED")
    private String state;

}
