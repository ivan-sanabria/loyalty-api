/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.loyalty.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class to encapsulate the redeem data supported by the loyalty endpoints.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Redemption status data transfer object used to establish communication between the clients " +
        "and backend services.", name = "Redemption")
public class RedemptionDto {

    /**
     * Unique distributed identifier of the redemption.
     */
    @Schema(description = "Unique distributed identifier of the redemption.",
            example = "123e4567-e89b-12d3-a456-426655440000")
    private String uuid;

    /**
     * Date the redemption expires.
     */
    @Schema(description = "Unix time when the redemption expires.",
            example = "1563485352883")
    private Long expiration;

    /**
     * Number of drinks required to get a reward.
     */
    @Schema(description = "Number of drinks required to get a reward.",
            example = "10")
    private Integer number;

    /**
     * State of the redemption.
     */
    @Schema(description = "State of the redemption.",
            example = "DELETED")
    private String state;

    /**
     * Unique distributed identifier of place that offers the redemption.
     */
    @Schema(description = "Unique distributed identifier of the place that offers the redemption.",
            example = "123e4567-e89b-12d3-a456-426655440000")
    private String placeId;

}
