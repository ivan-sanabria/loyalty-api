/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.loyalty.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * Class to encapsulate the number of drinks the user have been taken on specific redemption.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.1.0
 */
@Data
@DynamoDBTable(tableName = "kaffeeland-loyalty-status")
public class Status {

    /**
     * Compose key for loyalty card status.
     */
    @Id
    @DynamoDBIgnore
    private StatusId id;

    /**
     * Number of drinks taken by an user on specific redemption.
     */
    @DynamoDBAttribute
    private Integer number;

    /**
     * Retrieves the unique distributed identifier of the compose id.
     *
     * @return String representing the unique distributed identifier.
     */
    @DynamoDBHashKey(attributeName = "uuid")
    public String getUuid() {
        return (null != id) ? id.getUuid() : null;
    }

    /**
     * Set the unique distributed identifier of the compose id.
     *
     * @param uuid Unique distributed identifier.
     */
    public void setUuid(String uuid) {
        if (null == id) {
            id = new StatusId();
        }
        id.setUuid(uuid);
    }

    /**
     * Retrieves the unique distributed identifier of the redemption.
     *
     * @return String representing the unique distributed identifier of the redemption.
     */
    @DynamoDBRangeKey(attributeName = "redemptionId")
    public String getRedemptionId() {
        return (null != id) ? id.getRedemptionId() : null;
    }

    /**
     * Set the unique distributed identifier of the redemption to compose key.
     *
     * @param redemptionId Redemption distributed identifier to set to compose key.
     */
    public void setRedemptionId(String redemptionId) {
        if (null == id) {
            id = new StatusId();
        }
        id.setRedemptionId(redemptionId);
    }

}
