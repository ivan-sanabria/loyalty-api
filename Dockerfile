FROM adoptopenjdk/openjdk11:ubi

LABEL maintainer="icsanabriar@googlemail.com"

ARG AWS_ACCESS_KEY_ID=""
ENV AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID

ARG AWS_SECRET_ACCESS_KEY=""
ENV AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY

ARG JAR_FILE=target/loyalty-api-*.jar
ADD ${JAR_FILE} loyalty-api.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/loyalty-api.jar"]

EXPOSE 8080