# Loyalty API - Co-founder Project

version 1.2.0 - 04/12/2021

[![License](https://img.shields.io/badge/license-apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Build](https://img.shields.io/bitbucket/pipelines/ivan-sanabria/loyalty-api.svg)](http://bitbucket.org/ivan-sanabria/loyalty-api/addon/pipelines/home#!/results/branch/master/page/1)
[![PR](https://img.shields.io/bitbucket/pr/ivan-sanabria/loyalty-api.svg)](http://bitbucket.org/ivan-sanabria/loyalty-api/pull-requests)
[![Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_kaffeeland-loyalty-api&metric=alert_status)](https://sonarcloud.io/component_measures/metric/alert_status/list?id=ivan-sanabria_kaffeeland-loyalty-api)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_kaffeeland-loyalty-api&metric=bugs)](https://sonarcloud.io/component_measures/metric/bugs/list?id=ivan-sanabria_kaffeeland-loyalty-api)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_kaffeeland-loyalty-api&metric=coverage)](https://sonarcloud.io/component_measures/metric/coverage/list?id=ivan-sanabria_kaffeeland-loyalty-api)
[![Size](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_kaffeeland-loyalty-api&metric=ncloc)](https://sonarcloud.io/component_measures/metric/ncloc/list?id=ivan-sanabria_kaffeeland-loyalty-api)
[![TechnicalDebt](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_kaffeeland-loyalty-api&metric=sqale_index)](https://sonarcloud.io/component_measures/metric/sqale_index/list?id=ivan-sanabria_kaffeeland-loyalty-api)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_kaffeeland-loyalty-api&metric=vulnerabilities)](https://sonarcloud.io/component_measures/metric/vulnerabilities/list?id=ivan-sanabria_kaffeeland-loyalty-api)

## Specifications

Many customers are loyal to the coffee shops located in their areas. They would like to get rewarded after consuming x 
amount of products. The current status is that customers retrieve their loyalty card on paper, when they could stored 
on the mobile application. The current project covers the following uses cases:

1. Create loyalty card of an user for specific application.
2. Update loyalty card of an user for specific application.
3. Get loyalty card of an user for specific application.
4. Delete loyalty card of an user for specific application.
5. Create redemption based on the number of drinks a user consumes.
6. Update redemption data, in case the coffee shop make an error creating.
7. Mark as deleted a redemption that already expired or the coffee shop does not want to support.
8. Get the redemption in order to display how many drinks the user require to consume before get a reward.
9. Create a status that display how many drinks a consumer took for a specific redemption.
10. Update the status to increase / decrease the number of remaining drinks to get a reward.
11. Get the status in order to display how many drinks an user already consumed for a specific redemption. 

## Requirements

- JDK 11.x
- Maven 3.6.3
- IDE for JAVA (Eclipse, Netbeans, IntelliJ).
- Postman 7.27.x
- Serverless 1.83.x
- [Docker Desktop 2.3.x](https://www.docker.com/products/docker-desktop)
- [EB CLI 3.18.x](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3-install-advanced.html)

## Serverless Setup

In order to deploy the DynamoDB tables in AWS account is required to have an **AWS_ACCESS_KEY_ID** and **AWS_SECRET_ACCESS_KEY**.

To setup and configure serverless framework, for deploying the stack using AWS CloudFormation is 
required to execute the following commands on the root folder of the project on a terminal:

```bash
    npm install -g serverless
    serverless config credentials --provider aws --key ${AWS_ACCESS_KEY_ID} --secret ${AWS_SECRET_ACCESS_KEY} --profile demos
```

## Application Setup

To run the application using DynamoDB as datasource:

1. Set environment variables **AWS_ACCESS_KEY_ID** and **AWS_SECRET_ACCESS_KEY** in your .bash_profile to have access to DynamoDB.
2. Ensure that the given AWS credentials have the proper policies to upgrade tables, read and write data in DynamoDB.
3. Download the source code from repository.
4. Go to the root location of the source code.
5. Upgrade DynamoDB tables using serverless.

## Running Application inside IDE

To run the application on your IDE:

1. Verify the version of your JDK - 11.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Integrate your project with IDE:
    - [Eclipse](http://books.sonatype.com/m2eclipse-book/reference/creating-sect-importing-projects.html)
    - [Netbeans](http://wiki.netbeans.org/MavenBestPractices)
    - [IntelliJ]( https://www.jetbrains.com/idea/help/importing-project-from-maven-model.html)
5. Open IDE and run the class **Application.java**.

## Running Application on Terminal

To run the application on terminal with Maven:

1. Verify the version of your JDK - 11.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean verify
    java -jar target/loyalty-api-1.2.0.jar
```

## Running Unit Tests on Terminal

To run the unit tests on terminal:

1. Verify the version of your JDK - 11.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean test
```

## Running Application using Docker

To run the application on terminal with docker:

1. Verify the version of your JDK - 11.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Verify the version of Docker - 19.x or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean package
    docker build --build-arg AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} --build-arg AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} -t kaffeeland-loyalty-api .
    docker run -p 8080:8080 kaffeeland-loyalty-api
```

## Check Application Test Coverage using Jacoco

To verify test coverage on terminal:

1. Verify the version of your JDK - 11.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean verify
    open target/site/jacoco/index.html
```

## Testing using Postman Collection

To test using the postman collection:

1. Run the application inside IDE or Terminal.
2. Open the AWS console on DynamoDB > Tables > kaffeeland-loyalty-card > Items
3. Import the postman collection **loyalty-api-collection.json**.
4. Import the postman environment **loyalty-api-environment.json**.
5. Go to folder **Loyalty Card**.
6. Open **Create Loyalty Card** request.
7. Send the POST request.
8. Validate the Tests Results are green. 
9. Go to AWS console and refresh the items.
10. Copy the uuid of the new item.
11. Paste the value at the postman environment variable card_id.
12. Open **Get Loyalty Card** request.
13. Send the GET request.
14. Validate the Tests Results are green. 
15. Open **Update Loyalty Card** request.
16. Send the PUT request.
17. Validate the Tests Results are green. 
18. Open **Delete Loyalty Card** request.
19. Send the DELETE request.
20. Validate the Tests Results are green. 

* Please repeat similar process with the other two folders **Redemption** and **Status**.

## List Dependency Licenses

To generate list of dependency licenses on terminal:

1. Verify the version of your JDK - 11.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn project-info-reports:dependencies
    open target/site/dependencies.html
```

## Upgrade DynamoDB Tables on AWS using Serverless

After see the test coverage and generate files let's proceed to upgrade the DynamoDB tables to store the loyalty data:

```bash
    serverless deploy -v --aws-profile demos
```

## Continuous Deployment using Bitbucket Pipelines and Elastic Beanstalk

Deployment to production environment is already configured on **bitbucket-pipelines.yml**. This would create a new environment
on every deployment. 

To save cost on AWS it is recommended to terminate the environment after the exercise is covered:

```bash
    cd deployment
    eb terminate kaffeeland-loyalty-api-production
```

## API Documentation

To see swagger documentation:

1. Open a browser
2. Go to http://${URL}/swagger-ui.html

# Contact Information

Email: icsanabriar@googlemail.com